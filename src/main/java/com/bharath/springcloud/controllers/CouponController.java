package com.bharath.springcloud.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bharath.springcloud.model.Coupon;
import com.bharath.springcloud.repos.CouponRepo;

@RestController
@RequestMapping("/couponapi")
public class CouponController {

	@Autowired
	CouponRepo repo;
	
	@RequestMapping(value="/coupons",method=RequestMethod.POST)
	//@RequestBody : incoming josn will be utilized into Coupon
	public Coupon create(@RequestBody Coupon coupon) {
		
		return repo.save(coupon);
	}
	
	@RequestMapping(value="/coupons/{code}",method=RequestMethod.GET)
	//here we user code as a placeholder
	public Coupon getCoupon(@PathVariable("code") String code) {
		return repo.findByCode(code);
	}
	
}
